package com.controller;

import com.DAO.*;
import com.Entity.User;
import com.Service.RoleServiceImpl;
import com.Service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class Login{
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private RoleServiceImpl roleService;

    @RequestMapping(value = "/login")
    protected void login(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        final String username = req.getParameter("username");
        final String password = req.getParameter("password");

        User user = userService.findByLogin(username);
        if (user.getLogin().isEmpty() || !user.getPassword().equals(password)) {//проверки на нулл
            resp.sendRedirect("/login.jsp");
            return;
        }
        req.getSession().setAttribute("username", username);
        if (user.getRole().getName().equals("user")) {
            req.getRequestDispatcher("/homeuser.jsp").forward(req, resp);
            return;
        }
        if (user.getRole().getName().equals("admin")) {
            req.getRequestDispatcher("/homeadmin.jsp").forward(req, resp);
            return;
        }
        resp.sendRedirect("/login.jsp");
    }
}
