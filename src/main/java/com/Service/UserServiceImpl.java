package com.Service;

import com.DAO.JdbcUserDao;
import com.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
//@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private JdbcUserDao jdbcUserDao;

    @Override
//    @Transactional
    public void create(User user) {
        jdbcUserDao.create(user);
    }

    @Override
    public void update(User user) {
        jdbcUserDao.update(user);
    }

    @Override
//    @Transactional
    public void remove(User user) {
        jdbcUserDao.remove(user);
    }

    @Override
//    @Transactional
    public List<User> findAll() {
        return jdbcUserDao.findAll();
    }

    @Override
    public User findByLogin(String login) {
        return jdbcUserDao.findByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        return jdbcUserDao.findByEmail(email);
    }

    public void setJdbcUserDao(JdbcUserDao jdbcUserDao) {
        this.jdbcUserDao = jdbcUserDao;
    }
}
