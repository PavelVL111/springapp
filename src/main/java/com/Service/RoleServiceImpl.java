package com.Service;

import com.DAO.JdbcRoleDao;
import com.Entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
//@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    private JdbcRoleDao jdbcRoleDao;

    @Override
//    @Transactional
    public void create(Role role) {
        jdbcRoleDao.create(role);
    }

    @Override
    public void update(Role role) {
        jdbcRoleDao.update(role);
    }

    @Override
//    @Transactional
    public void remove(Role role) {
        jdbcRoleDao.remove(role);
    }

    @Override
    public Role findByName(String name) {
        return jdbcRoleDao.findByName(name);
    }

    public void setJdbcRoleDao(JdbcRoleDao jdbcRoleDao) {
        this.jdbcRoleDao = jdbcRoleDao;
    }
}
