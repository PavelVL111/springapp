package com.handlertag;


import com.DAO.JdbcUserDao;
import com.Entity.User;
import com.Service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import javax.servlet.jsp.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.jsp.tagext.SimpleTagSupport;


public class TagHandler extends SimpleTagSupport {
    @Autowired
    UserServiceImpl userService;

    public void doTag() throws JspException, IOException {

        List<User> list = userService.findAll();
        JspWriter out = getJspContext().getOut();

        for (User user : list) {
            out.print("<tr><td nowrap>" + user.getLogin() +
                    "</td><td nowrap>" + user.getFirstName() +
                    "</td><td nowrap>" + user.getLastName() +
                    "</td><td>" + getAge(user.getBirthDay()) +
                    "</td><td>" + user.getRole().getName() +
                    "</td><td nowrap><a href=\"sendonaddchange?login=" +
                    user.getLogin() + "&act=Edit\">Edit</a> " +
                    "<a href=\"#\" id=\"" +
                    user.getLogin() + "\" name=\"show\" class=\"show\" value=\"/login=" +
                    user.getLogin() + "\" onclick=\"return false;\">Delete</a></td></tr>");
        }
    }

    public static Integer getAge(String strBirthday)
    {
        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd.mm.yyyy", Locale.getDefault());
        Date birthday = null;
        try {
            birthday = oldDateFormat.parse(strBirthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.setTime(birthday);
        dob.add(Calendar.DAY_OF_MONTH, -1);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age;
    }
}
