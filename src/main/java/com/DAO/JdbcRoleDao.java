package com.DAO;

import com.Entity.Role;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import com.SessionFactory.HibernateSessionFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcRoleDao extends AbstractjdbcDao implements RoleDao {
    @Autowired
    private SessionFactory sessionFactory;

    private Properties propertiesJdbc;
    static final Logger logger = Logger.getLogger(JdbcRoleDao.class);
    Properties propertiesLog = new Properties();
    InputStream is = getClass().getResourceAsStream("/log4j.properties");
    public JdbcRoleDao() {
//        PropertyConfigurator.configure("Servlet/src/main/resources/log4j.properties");
//        try {
//            propertiesLog.load(is);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                is.close();
//            }
//            catch (Exception e) {
//                // ignore this exception
//            }
//        }
//        PropertyConfigurator.configure(propertiesLog);
    }

    @Override // разобраться
    final Connection createConnection() {
        return getConnection(propertiesJdbc.getProperty("db.driver"),
                propertiesJdbc.getProperty("db.url"),
                propertiesJdbc.getProperty("db.username"),
                propertiesJdbc.getProperty("db.password"));
    }

    static Connection getConnection(final String property, // разобраться
                                    final String property2,
                                    final String property3,
                                    final String property4) {
        try {
            Class.forName(property);
            return DriverManager.getConnection(property2,
                    property3,
                    property4);
        } catch (SQLException | ClassNotFoundException e) {
            logger.error(e.getMessage());
            throw new RuntimeException();
        }
    }

    @Override
    public final void create(final Role role) {
        sessionFactory.getCurrentSession().save(role);
    }

    @Override
    public final void update(final Role role) {
        sessionFactory.getCurrentSession().update(role);
    }

    @Override
    public final void remove(final Role role) {
        Role roleGet = (Role) sessionFactory.getCurrentSession().load(Role.class, role.getId());
        if (null != roleGet) {
            this.sessionFactory.getCurrentSession().delete(roleGet);
        }
    }

    @Override
    public final Role findByName(final String name) {
        return (Role) sessionFactory.getCurrentSession().createQuery("from Role where name = '" + name + "'")
                .list().get(0);
//
//        Session session = sessionFactory.openSession();
//        session.beginTransaction();
//        Role role = null;
//        Query queryResult = session.createQuery("from Role where name = '" + name + "'");
//        List listRole = queryResult.list();
//        if (listRole.size() > 0){
//            role = (Role) listRole.get(0);
//        }
//        session.getTransaction().commit();
//        session.close();
//        return role;
    }

    public final Role findByID(final long id) {
        return (Role) sessionFactory.getCurrentSession().get(
                Role.class, id);
    }

}
