package com.DAO;

import com.Entity.User;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import java.sql.Connection;
import java.util.List;
import java.util.Properties;
import com.SessionFactory.HibernateSessionFactory;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import static com.DAO.JdbcRoleDao.getConnection;

@Repository
public class JdbcUserDao extends AbstractjdbcDao implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    private Properties propertiesJdbc;
//    Properties propertiesLog = new Properties();
//    InputStream is = getClass().getResourceAsStream("/log4j.properties");

    static final Logger logger = Logger.getLogger(JdbcUserDao.class);

    public JdbcUserDao() {
//        PropertyConfigurator.configure("F:/JavaProjects/Servlet/src/main/resources/log4j.properties");

//        try {
//            propertiesLog.load(is);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                is.close();
//            }
//            catch (Exception e) {
//                // ignore this exception
//            }
//        }
//        PropertyConfigurator.configure(propertiesLog);
//        PropertyConfigurator.configure("Servlet/src/main/resources/log4j.properties");

    }

    @Override
    public final Connection createConnection() { // разобраться
        logger.info("Create connection");
        return getConnection(propertiesJdbc.getProperty("db.driver"),
                propertiesJdbc.getProperty("db.url"),
                propertiesJdbc.getProperty("db.username"),
                propertiesJdbc.getProperty("db.password"));
    }

    @Override
    public final void create(final User user) { // разобраться
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public final void update(final User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public final void remove(final User user) {
        User userGet = (User) sessionFactory.getCurrentSession().load(User.class, user.getId());
        if (null != userGet) {
            this.sessionFactory.getCurrentSession().delete(userGet);
        }
    }
    @SuppressWarnings("unchecked")
    @Override
    public final List<User> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from USER")
                .list();
    }

    @Override
    public final User findByLogin(final String login) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        User user = null;
        Query queryResult = session.createQuery("from User where login = '" + login + "'");
        List listUsers = queryResult.list();
        if (listUsers.size() > 0){
            user = (User) listUsers.get(0);
        }
        session.getTransaction().commit();
        session.close();
//        HibernateSessionFactory.shutdown();
        return user;
    }

    @Override
    public final User findByEmail(final String email) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        User user = null;
        Query queryResult = session.createQuery("from User where email = '" + email + "'");
        List listUsers = queryResult.list();
        if (listUsers.size() > 0){
            user = (User) listUsers.get(0);
        }
        session.getTransaction().commit();
        session.close();
//        HibernateSessionFactory.shutdown();
        return user;
    }


    public final void ch() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.createSQLQuery("ALTER TABLE ROLE ADD FOREIGN KEY (IDROLE) REFERENCES ROLE (IDROLE);");
        session.getTransaction().commit();
        session.close();
    }

//    public final void ch() {
//        try (Connection connection = createConnection();
//             Statement statement = connection.createStatement()) {
//            statement.execute("ALTER TABLE ROLE ALTER COLUMN ID BIGINT NOT NULL AUTO_INCREMENT;");
//        } catch (SQLException e) {
//            logger.error(e.getMessage());
//        }
//    }

//    public void createTable() {
//        try (Connection connection = createConnection();
//             Statement statement = connection.createStatement()) {
//            statement.execute("CREATE TABLE USER\n" +
//                    "(\n" +
//                    "    ID BIGINT PRIMARY KEY,\n" +
//                    "    LOGIN VARCHAR(255),\n" +
//                    "    PASSWORD VARCHAR(255),\n" +
//                    "    EMAIL VARCHAR(255),\n" +
//                    "    FIRSTNAME VARCHAR(255),\n" +
//                    "    LASTNAME VARCHAR(255),\n" +
//                    "    BIRTHDAY VARCHAR(255)\n" +
//                    ");");
//            statement.execute("    CREATE TABLE ROLE\n" +
//                    "            (\n" +
//                    "                    ID BIGINT PRIMARY KEY,\n" +
//                    "                    NAME VARCHAR(255),\n" +
//                    ");");
//        } catch (SQLException e) {
//            logger.error(e.getMessage());
//        }
//    }

}
